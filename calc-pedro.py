from tkinter import *

e = ''
root = Tk()
root.title('Calculadora')
frame = Frame(root)
frame2 = Frame(root)
frame.pack(side=TOP)
frame2.pack(side=TOP)
fundo = Canvas(root, bg='pink')
rotulo = Label(frame, text='', width=20, height=2, relief=RIDGE, font='monospace 22 bold', fg='#d31733')
rotulo.pack(fill=Y)

#Botões -> Números
botao0 = Button(frame2, text='0', bd=3, padx=1, pady=8, width=8, height=2) 
botao1 = Button(frame2, text='1', bd=3, padx=1, pady=8, width=8, height=2)
botao2 = Button(frame2, text='2', bd=3, padx=1, pady=8, width=8, height=2)
botao3 = Button(frame2, text='3', bd=3, padx=1, pady=8, width=8, height=2)
botao4 = Button(frame2, text='4', bd=3, padx=1, pady=8, width=8, height=2)
botao5 = Button(frame2, text='5', bd=3, padx=1, pady=8, width=8, height=2)
botao6 = Button(frame2, text='6', bd=3, padx=1, pady=8, width=8, height=2)
botao7 = Button(frame2, text='7', bd=3, padx=1, pady=8, width=8, height=2)
botao8 = Button(frame2, text='8', bd=3, padx=1, pady=8, width=8, height=2)
botao9 = Button(frame2, text='9', bd=3, padx=1, pady=8, width=8, height=2)

#Botões -> Operações
botao_adic = Button(frame2, text='+', bd=3, padx=1, pady=8, width=8, height=2 )
botao_subt = Button(frame2, text='-', bd=3, padx=1, pady=8, width=8, height=2 )
botao_mult = Button(frame2, text='x', bd=3, padx=1, pady=8, width=8, height=2 )
botao_divi = Button(frame2, text='/', bd=3, padx=1, pady=8, width=8, height=2 )
botao_igua = Button(frame2, text='=', bd=3, padx=1, pady=8, width=8, height=2 )
botao_abre = Button(frame2, text='(', bd=3, padx=1, pady=8, width=8, height=2 )
botao_fech = Button(frame2, text=')', bd=3, padx=1, pady=8, width=8, height=2 )
botao_pont = Button(frame2, text='.', bd=3, padx=1, pady=8, width=8, height=2 )
#Botões -> Especiais
botao_limp = Button(frame2, text='C', bd=3, padx=1, pady=8, width=8, height=2, fg='white', bg='#d31733' )
botao_sair = Button(frame2, text='Sair', bd=3, padx=1, pady=8, width=8, height=2, command=root.quit)


#Posição
#COLUNA 0
botao_limp.grid(row=0,column=0)
botao7.grid(row=1,column=0)
botao4.grid(row=2,column=0)
botao1.grid(row=3,column=0)
botao_abre.grid(row=4, column=0, columnspan=2, sticky="NESW")
botao_sair.grid(row=5,column=0, columnspan=4, sticky="NESW")

#COLUNA 1
botao_adic.grid(row=0,column=1)
botao8.grid(row=1,column=1)
botao5.grid(row=2,column=1)
botao2.grid(row=3,column=1)
botao_fech.grid(row=4,column=2, columnspan=2, sticky="NESW")

#COLUNA 2
botao_subt.grid(row=0,column=2)
botao9.grid(row=1,column=2)
botao6.grid(row=2,column=2)
botao3.grid(row=3,column=2)

#COLUNA 3
botao_mult.grid(row=0,column=3)
botao_divi.grid(row=1,column=3)
botao_pont.grid(row=2,column=3)
botao_igua.grid(row=3,column=3)

#RETORNO-Numero

def numero(ev):
    global e, rotulo
    if len(e) > 1 and e[1].isdigit():
        e = e.lstrip('0')
        rotulo.config(text=e)
    if ev.widget==botao0:
        e+='0'
        rotulo.config(text=e)
    elif ev.widget==botao1:
        e+='1'
        rotulo.config(text=e)
    elif ev.widget==botao2:
        e+='2'
        rotulo.config(text=e)
    elif ev.widget==botao3:
        e+='3'
        rotulo.config(text=e)
    elif ev.widget==botao4:
        e+='4'
        rotulo.config(text=e)
    elif ev.widget==botao5:
        e+='5'
        rotulo.config(text=e)
    elif ev.widget==botao6:
        e+='6'
        rotulo.config(text=e)
    elif ev.widget==botao7:
        e+='7'
        rotulo.config(text=e)
    elif ev.widget==botao8:
        e+='8'
        rotulo.config(text=e)
    else:
        e+='9'
        rotulo.config(text=e)

def operacao(ev):
    global e, rotulo
    if ev.widget==botao_adic:
        e+='+'
        rotulo.config(text=e)
    elif ev.widget==botao_subt:
        e+='-'
        rotulo.config(text=e)
    elif ev.widget==botao_mult:
        e+='*'
        rotulo.config(text=e)
    elif ev.widget==botao_divi:
        e+='/'
        rotulo.config(text=e)
    elif ev.widget==botao_pont:
        e+= '.'
        rotulo.config(text=e)


def parenteses(ev):
    global e, rotulo
    if ev.widget==botao_abre:
        e+='('
        rotulo.config(text=e)
    elif ev.widget==botao_fech:
        e+=')'
        rotulo.config(text=e)


def finalizar(ev):
    global e, rotulo
    try:
        r = eval(e)
        e=str(r)
        rotulo.config(text=e)
    except:
        rotulo.config(text='Erro!')

def limpar(ev): #Limpar o cálculo que foi feito
    global e, rotulo
    e = '' 
    rotulo.config(text=e)

#Obrigação

botao0.bind('<Button-1>', numero)
botao1.bind('<Button-1>', numero)
botao2.bind('<Button-1>', numero)
botao3.bind('<Button-1>', numero)
botao4.bind('<Button-1>', numero)
botao5.bind('<Button-1>', numero)
botao6.bind('<Button-1>', numero)
botao7.bind('<Button-1>', numero)
botao8.bind('<Button-1>', numero)
botao9.bind('<Button-1>', numero)

botao_adic.bind('<Button-1>', operacao)
botao_subt.bind('<Button-1>', operacao)
botao_mult.bind('<Button-1>', operacao)
botao_divi.bind('<Button-1>', operacao)
botao_pont.bind('<Button-1>', operacao)

botao_abre.bind('<Button-1>', parenteses)
botao_fech.bind('<Button-1>', parenteses)

botao_igua.bind('<Button-1>', finalizar)
botao_limp.bind('<Button-1>', limpar)

#Abrir
fundo.pack()  
root.mainloop()